from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
import os
from airflow.providers.amazon.aws.transfers.local_to_s3 import LocalFilesystemToS3Operator

# Configuration Parameters
BASE_DIRECTORY = '/opt/results/'
PROCESSED_FILES_RECORD_PATH = '/opt/airflow/processed_files.txt'
BUCKET_NAME = ''
PREFIX = 'xml_files/'

default_args = {
    'owner': 'airflow',
    'start_date': datetime(2023, 1, 1),
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    'monitor_and_upload_to_s3',
    default_args=default_args,
    description='Recursively check for test_result.xml files',
    schedule_interval=None,  # Set to None for a one-time execution
)

def process_file(file_path):
    # Perform any desired action with the file_path
    print(f'Processing test_result.xml file: {file_path}')

def update_processed_files(processed_files, file_path):
    processed_files.add(file_path)
    with open(PROCESSED_FILES_RECORD_PATH, 'w') as file:
        file.write('\n'.join(processed_files))

def check_test_results(**kwargs):
    processed_files = set()

    # Load the list of processed files
    if os.path.exists(PROCESSED_FILES_RECORD_PATH):
        with open(PROCESSED_FILES_RECORD_PATH, 'r') as file:
            processed_files.update(line.strip() for line in file)

    # Recursively search for test_result.xml files
    xml_files_found = False
    for root, dirs, files in os.walk(BASE_DIRECTORY):
        for file in files:
            if file == 'test_result.xml':
                file_path = os.path.join(root, file)

                # Check if the file has already been processed
                if file_path not in processed_files:
                    process_file(file_path)
                    update_processed_files(processed_files, file_path)
                    xml_files_found = True

    if not xml_files_found:
        raise ValueError('No XML files found during processing. Task failed.')

    # Return the file path in a dictionary to pass through XCom
    return {'file_path': file_path}

# Define the PythonOperator to print file path and processed_files.txt contents
def print_file_path_and_processed_files(**kwargs):
    file_path = kwargs['ti'].xcom_pull(task_ids='check_test_results_task')['file_path']
    print(f'Full path of processed XML file: {file_path}')

    # Print the contents of processed_files.txt
    with open(PROCESSED_FILES_RECORD_PATH, 'r') as file:
        processed_files_content = file.read()
        print(f'Contents of processed_files.txt:\n{processed_files_content}')


check_results_task = PythonOperator(
    task_id='check_test_results_task',
    python_callable=check_test_results,
    provide_context=True,  # Enable providing context to the function
    dag=dag,
)

print_file_task = PythonOperator(
    task_id='print_file_path_and_processed_files',
    python_callable=print_file_path_and_processed_files,
    provide_context=True,
    dag=dag,
)

# Define the S3CreateObjectOperator to upload identified XML file to S3
upload_to_s3_task = LocalFilesystemToS3Operator(
    task_id='upload_to_s3_task',
    aws_conn_id='aws_default',
    dest_bucket=BUCKET_NAME,
    dest_key=f'{PREFIX}test_result.xml',  # Set the desired key (S3 object name)
    filename="{{ task_instance.xcom_pull(task_ids='check_test_results_task')['file_path'] }}",  # Use XCom to get file path
    replace=True,  # Overwrite the file if it already exists in S3
    dag=dag,
)

check_results_task >> print_file_task >> upload_to_s3_task
