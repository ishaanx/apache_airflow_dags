# Airflow DAG Documentation: monitor_and_upload_to_s3

## Overview

The `check_test_results` DAG is designed to recursively search for `test_result.xml` files in the `/opt/results/` directory, process them, and upload them to an S3 bucket. The DAG includes tasks for checking and processing XML files, printing file paths and processed files, and uploading to S3.

## DAG Structure

### 1. check_test_results_task

- **Task Type:** `PythonOperator`
- **Description:** Recursively searches for `test_result.xml` files in the `/opt/results/` directory. If found, processes the file by printing its path and updating the list of processed files in `processed_files.txt`.

### 2. print_file_path_and_processed_files

- **Task Type:** `PythonOperator`
- **Description:** Prints the full path of the processed XML file and the contents of the `processed_files.txt` file. This task is dependent on the success of the `check_test_results_task`.

### 3. upload_to_s3_task

- **Task Type:** `LocalFilesystemToS3Operator`
- **Description:** Uploads the processed XML file to an S3 bucket. Uses XCom templating to get the file path from the `check_test_results_task`.

## Configuration Parameters

- **BASE_DIRECTORY:** The base directory to recursively search for `test_result.xml` files.
- **PROCESSED_FILES_RECORD_PATH:** The path to the `processed_files.txt` file, which keeps a record of processed XML files.
- **S3_BUCKET_NAME:** The name of the S3 bucket for uploading the XML files.
- **S3_PREFIX:** The prefix to be used for S3 object keys.

## Default Arguments

- **owner:** The owner of the DAG.
- **start_date:** The date and time when the DAG should start running.
- **retries:** The number of retries for each task.
- **retry_delay:** The delay between task retries.

## Execution Schedule

The DAG is set to run once (`schedule_interval=None`). It can be triggered manually or integrated into a larger workflow.

## Usage

1. Ensure that Airflow is properly configured and the required plugins are installed.
2. Copy the DAG script into the Airflow DAGs directory.
3. Modify the configuration parameters according to your environment.
4. Ensure the following Airflow connections are set up:

    - **aws_default:** Connection to AWS S3. Ensure it includes AWS access key ID and secret access key.

## Dependencies

- Airflow Operators: `PythonOperator`, `LocalFilesystemToS3Operator`
- Airflow Connections: An AWS S3 connection with the ID 'aws_default'

![Alt text](image.png)

## Customization

Feel free to customize the DAG script based on your specific requirements. You can add or remove tasks, modify configuration parameters, or adjust the schedule interval to suit your workflow.